#ifndef MSRSD_STRING_H__
#define MSRSD_STRING_H__

#include <string>
#include <cstring>

using namespace std;

namespace msrsd
{

namespace util
{

namespace string
{

void multipleDigits(const char* str, char ch, char* output) {
	int x = strlen(str);
	strcpy(output, str);
	while ((x -= 3) > 0) {
		memmove(output + x + 1, output + x, strlen(output) - x + 1);
		output[x] = ch;
	}
}

} // namespace string
}
} // namespace msrsd
#endif
