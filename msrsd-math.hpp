#ifndef MSRSD_MATH_H__
#define MSRSD_MATH_H__

#include <string>
#include <vector>
#include <random>
#include <map>
#include <experimental/filesystem>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>

using namespace std;

namespace fs = std::experimental::filesystem;

namespace msrsd
{

namespace util
{

namespace math
{

/* return random number between a and b */
std::mt19937::result_type getRandomNumber(std::mt19937::result_type a, std::mt19937::result_type b = UINT_FAST32_MAX){
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(a,b); // distribution in range [1, 6]
    return dist(rng);
}

} // namespace math
}
} // namespace msrsd
#endif
