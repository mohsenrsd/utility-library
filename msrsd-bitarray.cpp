#include "msrsd-bitarray.h"

#include <iostream>
#include <cstring>

using namespace msrsd;

BitArray::BitArray():
    bit_buffer(NULL),num_of_bits(0)
{
}

BitArray::BitArray(BSize the_num_of_bits):
    num_of_bits(the_num_of_bits) {
    bit_buffer = new Byte[(BSize)ceil((float)num_of_bits / 8.f)];
}

BitArray::~BitArray() {
    if(bit_buffer){
        delete[] bit_buffer;
        bit_buffer = NULL;
    }
}

BitArray::BSize BitArray::sizeOfBytes()const {
    return static_cast<BSize>(ceil((float)num_of_bits / 8));
}

BitArray::BSize BitArray::numOfBits()const {
    return num_of_bits;
}

void BitArray::setBit(const BSize index, const bool value) {
    if(index > num_of_bits - 1){
        std::cerr << "fatal error: setBit index out of range." << std::endl;
        return;
    }
    if(value)
        bit_buffer[index / 8] |= 1 << (7 - index % 8);
    else
        bit_buffer[index / 8] &= ~(1 << (7 - index % 8));
}

bool BitArray::getBit(const BSize index) const{
    if(bit_buffer[index / 8] &  (1 << (7 - index % 8)))
        return true;
    return false;
}

BitArray::BSize BitArray::getBytes(BitArray::Byte** output_byte_buffer) const{
    *output_byte_buffer = bit_buffer;
    return (BSize)ceil((float)num_of_bits / 8);
}

void BitArray::resizeBit(BSize newSize){
    if(bit_buffer){
        delete[] bit_buffer;
        bit_buffer = NULL;
    }
    num_of_bits = newSize; 
    bit_buffer = new Byte[(BSize)ceil((float)newSize / 8.f)];
}

void BitArray::copyByte(const BitArray::Byte* buff, BitArray::BSize size){
    if( size > (BSize)ceil((float)num_of_bits / 8.f)){
        std::cerr << "fatal error: copyByte size out of range." << std::endl;
        return;
    }
    memcpy(bit_buffer, buff, (BSize)size);
}

void BitArray::setAll(bool bit){
    if(bit)
        memset(bit_buffer, 0xff, (BSize)ceil((float)num_of_bits / 8.f));
    else 
        memset(bit_buffer, 0x00, (BSize)ceil((float)num_of_bits / 8.f));
}

void BitArray::setLimit(const BSize limit){
    if(limit <= num_of_bits)
        num_of_bits = limit;
}

BitArray::BSize BitArray::count(const bool bit, const BSize startIndex, const BSize endIndex) const{
    BSize newEndIndex = (endIndex > num_of_bits - 1) ? num_of_bits - 1 : endIndex;
    BSize count = 0;
    for(BSize i = startIndex ; i <= newEndIndex ; i++){
        if(getBit(i) == bit)
            count++;
    }
    return count;
}

bool BitArray::operator[](int index) const {
    return getBit(index);
}

std::ostream& operator<< (std::ostream& out, BitArray& obj){
    out << std::endl;
    for(BitArray::BSize i = 0 ; i < obj.numOfBits() ; i++){
        char result = (obj.getBit(i) == true) ? '1' : '0';
        if(i != obj.numOfBits() - 1)
            out << result << ' ';
        else
            out << result;
    }
    out << std::endl;
    return out;
}
