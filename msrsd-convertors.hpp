#ifndef MSRSD_CONVERTORS_H__
#define MSRSD_CONVERTORS_H__

#include <string>
#include <experimental/filesystem>
#include <iostream>
#include <fstream>

using namespace std;

namespace msrsd
{
namespace util
{
namespace cvt
{
size_t stringToInt(const std::string &s){
    stringstream geek(s);
    size_t x = 0;
    geek >> x;
    return x;
}
string intToString(size_t i){
    std::ostringstream convert;
    convert << i;
    return convert.str();
}
} // namespace cvt
} // namespace util
} // namespace msrsd
#endif
