// A high level (C++ 98) class that works with bits.

#ifndef BIT_ARRAY_H_
#define BIT_ARRAY_H_

#include <cmath>
#include <iostream>

namespace msrsd {

class BitArray {

public:

    typedef size_t BSize;
    typedef unsigned char Byte;

    BitArray();

    BitArray(BSize the_num_of_bits);

    BitArray(BitArray& righBitArray);

    virtual ~BitArray();

    BSize sizeOfBytes()const;

    BSize numOfBits()const;

    void setBit(const BSize index, const bool value);
    
    bool getBit(const BSize index) const;

    BSize getBytes(Byte** output_byte_buffer) const;

    void resizeBit(BSize newSize);

    void copyByte(const Byte* buff, BSize size);

    void setAll(bool bit);

    void printBits()const;

    void setLimit(BSize limit);

    BSize count(const bool bit, const BSize startIndex, const BSize endIndex) const;

    bool operator[](int x) const;

    friend std::ostream& operator<< (std::ostream& out, BitArray& obj);

private:
    Byte* bit_buffer;
    BSize num_of_bits;
    bool assignBit;
};

std::ostream& operator<< (std::ostream& out, BitArray& obj);

}// namespace msrsd
#endif
