#ifndef MSRSD_GENERAL_H__
#define MSRSD_GENERAL_H__

#include "msrsd-convertors.hpp"

#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

namespace msrsd
{

namespace util
{

template <typename T1, typename T2>
class MapFile
{
public:

    static int write(const std::string &fname, std::map<T1, T2> &m){
        int count = 0;
        if (m.empty())
            return 0;
        fstream fs;
        fs.open(fname.c_str(), ios::out);
        if(!fs.good())
            return -errno;
        for(std::map<uint64_t, std::string>::iterator it = m.begin(); it != m.end(); it++) {
            fs << it->first << '=' << it->second.c_str() << endl;
            count++;
        }
        fs.close();
        return count;
    }

    static int read(const std::string &fname, std::map<T1, T2> &m){
        int count = 0;

        if(!util::filedir::fileExist(fname)) {
            return -1;
        }

        FILE *fp = fopen(fname.c_str(), "r");
        if (!fp)
            return -errno;

        m.clear();

        char *buf = 0;
        size_t buflen = 0;

        while(getline(&buf, &buflen, fp) > 0) {
            char *nl = strchr(buf, '\n');
            if (nl == NULL)
                continue;
            *nl = 0;

            char *sep = strchr(buf, '=');
            if (sep == NULL)
                continue;
            *sep = 0;
            sep++;

            std::string bufStr = buf;
            uint64_t s1 = msrsd::util::cvt::stringToInt(bufStr);
            std::string s2 = sep;

            (m)[s1] = s2;

            count++;
        }

        if (buf)
            free(buf);

        fclose(fp);
        return count;
    }
};

template <typename T>
std::vector<std::vector<T>> splitVector(const std::vector<T> &bigVector, int n){
    // determine number of sub-vectors of size n
    int size = (bigVector.size() - 1) / n + 1;

    // create array of vectors to store the sub-vectors
    std::vector<std::vector<T> > vec;
    vec.resize(size);

    // each iteration of this loop process next set of n elements
    // and store it in a vector at k'th index in vec
    for (int k = 0; k < size; ++k)
    {
        // get range for next set of n elements
        auto start_itr = std::next(bigVector.cbegin(), k*n);
        auto end_itr = std::next(bigVector.cbegin(), k*n + n);

        // allocate memory for the sub-vector
        vec[k].resize(n);

        // code to handle the last sub-vector as it might
        // contain less elements
        if (k*n + n > bigVector.size()) {
            end_itr = bigVector.cend();
            vec[k].resize(bigVector.size() - k*n);
        }

        // copy elements from the input range to the sub-vector
        std::copy(start_itr, end_itr, vec[k].begin());
    }

    return vec;
} // namespace util

}

} // namespace msrsd
#endif
