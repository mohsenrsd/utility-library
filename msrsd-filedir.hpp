#ifndef MSRSD_FILEDIR_H__
#define MSRSD_FILEDIR_H__

#include <string>
#include <map>
#include <experimental/filesystem>
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

namespace fs = std::experimental::filesystem;

namespace msrsd
{

namespace util
{

namespace filedir
{
// c++ 17 get all files in directory include subdirectories
std::vector<std::string> getDirectoryFiles(const std::string &dir, const std::vector<std::string> &extensions = {}) {
     std::vector<std::string> files;
    for(auto & p: fs::recursive_directory_iterator(dir))
    {
        if (fs::is_regular_file(p))
        {
            if (extensions.empty() || find(extensions.begin(), extensions.end(), p.path().extension().string()) != extensions.end())
            {
                files.push_back(p.path().string());
            }
        }
    }
    return files;
}

bool fileExist(const std::string &name){
    ifstream f(name.c_str());
    return f.good();
}

bool createDirectory(const string &dir_path){
    return fs::create_directories(dir_path);
}

std::size_t numofFilesInDirectory(std::experimental::filesystem::path path){
    using std::experimental::filesystem::directory_iterator;
    return std::distance(directory_iterator(path), directory_iterator{});
}

} // namespace filedir

}

} // namespace msrsd
#endif
